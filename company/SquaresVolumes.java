package com.company;

public class SquaresVolumes {

    public static void main(String[] args) {
        circle();
        rectangle();
        triangle();
        triangleGeron();
        trapezoid();
        ring();
        sphere();
        ellipse();
        halfSphere();
        parallelepiped();


    }

    static void circle() {
        double radius = 8;
        double pi = 3.14;
        double s = pi * (radius * radius);
        System.out.println("Площадь круга равна - " + s);
        System.out.println("-----------------------------------");
    }

    static void rectangle() {
        int a = 10;
        int b = 5;
        int r = a * b;
        System.out.println("Площадь прямоугольника равна - " + r);
        System.out.println("-----------------------------------");

    }

    static void triangle() {
        int a = 20;
        int h = 9;
        int s = a / 2 * h;
        System.out.println("Площадь треугольника по стороне и высоте равна - " + s);
        System.out.println("-----------------------------------");
    }

    static void triangleGeron() {
        byte a = 35;
        byte b = 45;
        byte c = 40;
        int hp = 100 / 2;
        double s = Math.sqrt(hp * (hp - a) * (hp - b) * (hp - c));
        System.out.println("Площадь треугольника по формуле Герона равна - " +s);
        System.out.println("-----------------------------------");
    }
    static void trapezoid() {
        byte a = 5;
        byte b = 7;
        byte h = 10;
        int s = ((a + b)/2)*h;
        System.out.println("площадь трапеции равна - " + s);
        System.out.println("-----------------------------------");
    }
    static void ring() {
        byte r = 5;
        short rr = 6;
        float pi = 3.14f;
        double s = pi*(rr*rr - r*r);
        System.out.println("площадь кольца равна - " + s);
        System.out.println("-----------------------------------");
        System.out.println("Далее - объемы.");
    }
    // далее - объемы

    static void sphere() {
        double pi = 3.14;
        double q = 2;
        double q1 = 3;
        double r = Math.pow(q,q1);
        double v = pi*r;
        System.out.println("Объем шара равен - " + v);
        System.out.println("-----------------------------------");

    }
    static void ellipse() {
        double pi = 3.14;
        double a = 2;
        double b = 3;
        double c = 4;
        double v = (4*pi/3)*a*b*c;
        System.out.println("Объем эллипса равен - " + v);
        System.out.println("-----------------------------------");

    }
    static void halfSphere() {
        double pi = 3.14;
        byte q = 5;
        byte q1 = 2;
        double h = Math.pow(q,q1);
        byte r = 6;
        double v = pi*h*(r-(h/3));
        System.out.println("Объем полусферы равен - " + v);
        System.out.println("-----------------------------------");
    }
    static void parallelepiped() {
        int h = 5;
        int c = 100;
        int v = h*c;
        System.out.println("Объем параллелепипеда равен - " + v);
        System.out.println("-----------------------------------");
    }

}