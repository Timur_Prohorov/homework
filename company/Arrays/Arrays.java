package com.company.Arrays;

public class Arrays {
    public static void main(String[] args) {
        int[][] zeros = new int[8][];
        zeros[0] = new int[1];
        zeros[1] = new int[2];
        zeros[2] = new int[3];
        zeros[3] = new int[4];
        zeros[4] = new int[4];
        zeros[5] = new int[3];
        zeros[6] = new int[2];
        zeros[7] = new int[1];
        for(int i = 0; i<zeros.length; i++){
            for(int j = 0; j<zeros[i].length; j++){
                System.out.print(zeros[i][j] + " ");
            }
            System.out.println();
        }
    }
}